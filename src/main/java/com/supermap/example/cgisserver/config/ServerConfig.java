package com.supermap.example.cgisserver.config;

import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ServerConfig implements ApplicationListener<WebServerInitializedEvent> {

    private static String port;

    public static String getPort() {
        return port;
    }

    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        port = String.valueOf(event.getWebServer().getPort());
    }
}
