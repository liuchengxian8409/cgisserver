package com.supermap.example.cgisserver;

import com.supermap.example.cgisserver.config.ServerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.net.InetAddress;

@SpringBootApplication
public class CgisserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(CgisserverApplication.class, args);

        // SpringBoot 程序启动时自动打开 Swagger 测试页面
        try {
            // 获取本机 IP 和 端口
            InetAddress inetAddress = InetAddress.getLocalHost();
            String hostAddress = inetAddress.getHostAddress();
            String port = ServerConfig.getPort();

            // 打开 Swagger 测试页面
            Runtime.getRuntime().exec("cmd /c start http://".concat(hostAddress).concat(":").concat(port).concat("/swagger-ui/index.html"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
