package com.supermap.example.cgisserver.controller.dataServices;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.supermap.data.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.ArrayList;

@Api(tags = "数据服务")
@RestController
@RequestMapping("/data")
public class DataServiceController {

    private Workspace workspace;

    @Operation(summary = "打开工作空间",
            description = "打开文件型工作空间"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "workspacePath", value = "文件型工作空间路径", dataType = "String", dataTypeClass = String.class)
    })
    @GetMapping("/openWorkspace")
    public boolean openWorkspace(String workspacePath) {
        if (workspace != null) {
            workspace.close();
            workspace.dispose();
            workspace = null;
        }

        File file = new File(workspacePath);
        String workspaceName = file.getName();
        String[] splitted = workspaceName.split("\\.");
        workspaceName = "";
        for (int i = 0; i < splitted.length - 1; i++) {
            workspaceName = workspaceName.concat(splitted[i]);
            if (i < splitted.length - 2) {
                workspaceName = workspaceName.concat(".");
            }
        }

        WorkspaceConnectionInfo workspaceConnectionInfo = new WorkspaceConnectionInfo(workspacePath);
        workspaceConnectionInfo.setName(workspaceName);

        workspace = new Workspace();
        return workspace.open(workspaceConnectionInfo);
    }

    @Operation(summary = "获取数据源列表",
            description = "获取当前工作空间内的数据源列表"
    )
    @GetMapping("/getDatasourceNames")
    public ArrayList<String> getDatasourceNames() {
        ArrayList<String> datasourceNames = new ArrayList<>();
        if (workspace != null) {
            Datasources datasources = workspace.getDatasources();
            for (int i = 0; i < datasources.getCount(); i++) {
                Datasource datasource = datasources.get(i);
                String datasourceAlias = datasource.getAlias();
                datasourceNames.add(datasourceAlias);
            }
        }
        return datasourceNames;
    }

    @Operation(summary = "获取数据集名称列表",
            description = "根据输入的数据源名称获取数据集名称列表"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "datasourceName", value = "数据源名称", dataType = "String", dataTypeClass = String.class)
    })
    @GetMapping("/getDatasetNames")
    public ArrayList<String> getDatasetNames(String datasourceName) {
        ArrayList<String> datasetNames = new ArrayList<>();
        if (workspace != null) {
            Datasources datasources = workspace.getDatasources();
            if (datasources.contains(datasourceName)) {
                Datasource datasource = datasources.get(datasourceName);
                Datasets datasets = datasource.getDatasets();
                for (int i = 0; i < datasets.getCount(); i++) {
                    Dataset dataset = datasets.get(i);
                    datasetNames.add(dataset.getName());
                }
            }
        }
        return datasetNames;
    }

    @Operation(summary = "获取指定数据集记录",
            description = "根据输入的数据源名称、数据集名称获取记录"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "datasourceName", value = "数据源名称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "datasetName", value = "数据集名称", dataType = "String", dataTypeClass = String.class)
    })
    @GetMapping("/getRecordset")
    public JSONArray getRecordset(String datasourceName, String datasetName) {
        JSONArray jsonArray = new JSONArray();
        if (workspace != null) {
            Datasources datasources = workspace.getDatasources();
            if (datasources.contains(datasourceName)) {
                Datasource datasource = datasources.get(datasourceName);
                Datasets datasets = datasource.getDatasets();
                if (datasets.contains(datasetName)) {
                    Dataset dataset = datasets.get(datasetName);
                    if (dataset instanceof DatasetVector) {
                        DatasetVector datasetVector = (DatasetVector) dataset;
                        Recordset recordset = datasetVector.getRecordset(false, CursorType.STATIC);
                        FieldInfos fieldInfos = recordset.getFieldInfos();
                        for (int i = 0; i < recordset.getRecordCount(); i++) {
                            recordset.moveTo(i);
                            JSONObject subJsonObject = new JSONObject();
                            for (int j = 0; j < fieldInfos.getCount(); j++) {
                                FieldInfo fieldInfo = fieldInfos.get(j);
                                String fieldName = fieldInfo.getName();
                                Object fieldValue = recordset.getFieldValue(fieldName);
                                if (fieldInfo.isSystemField() && fieldInfo.getType() == FieldType.LONGBINARY) {
                                    fieldValue = Toolkit.GeometryToGeoJson(recordset.getGeometry());
                                }
                                subJsonObject.put(fieldName, fieldValue);
                            }
                            jsonArray.add(subJsonObject);
                        }
                        recordset.close();
                        recordset.dispose();
                    }
                }
            }
        }
        return jsonArray;
    }
}
