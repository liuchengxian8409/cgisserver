package com.supermap.example.cgisserver.controller.test;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Api(tags = "测试")
@RestController
@RequestMapping("/test")
public class TestController {

    @Operation(summary = "测试 Get 接口",
            description = "该接口主要用于测试环境是否正常"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "who", value = "测试参数", dataType = "String", dataTypeClass = String.class)
    })
    @GetMapping("/get")
    public String hello(@RequestParam(value = "who", defaultValue = "world") String who) {
        return "Hello ".concat(who);
    }

    @Operation(summary = "测试 Post 接口",
            description = "该接口主要用于测试环境是否正常"
    )
    @PostMapping("/post")
    public TestBody hello(@Validated @RequestBody TestBody value) {
        return value;
    }

}
