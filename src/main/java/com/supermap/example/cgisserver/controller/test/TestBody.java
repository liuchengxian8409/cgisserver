package com.supermap.example.cgisserver.controller.test;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@ApiModel(value = "TestBody", description = "测试请求体")
@Data // 生成setter/getter、equals、canEqual、hashCode、toString方法，如为final属性，则不会为该属性生成setter方法
@AllArgsConstructor // 生成包含类中所有字段的构造方法
@Slf4j // 生成log变量，严格意义来说是常量
public class TestBody {

    @ApiModelProperty(name = "title", value = "测试标题", example = "标题", required = true)
    private String title;

    @ApiModelProperty(name = "value", value = "测试值", example = "值")
    private String value;

}
